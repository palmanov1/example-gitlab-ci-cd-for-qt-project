#include <QObject>
#include <QTest>
#include <QtTest>

class SampleTestClass : public QObject {
  Q_OBJECT
private slots:
  void sampleTestFunction();
};

void SampleTestClass::sampleTestFunction() { QVERIFY(1 == 1); }

QTEST_MAIN(SampleTestClass)
#include "SampleTest.moc" // clazy:skip
