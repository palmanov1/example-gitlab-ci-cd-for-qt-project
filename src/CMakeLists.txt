#scan for all .h files
file(GLOB_RECURSE AUTOMATIC_SCANNED_H_FILES CONFIGURE_DEPENDS "*.h")

#scan for all cxx/cpp files
file(GLOB_RECURSE AUTOMATIC_SCANNED_CXX_FILES CONFIGURE_DEPENDS  "*.cpp" "*.cxx")

#add to IDE
source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} PREFIX "Header Files" FILES ${AUTOMATIC_SCANNED_H_FILES})
source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} PREFIX "Source Files" FILES ${AUTOMATIC_SCANNED_CXX_FILES})

#macro that search all subdirectories that include .h files
MACRO(HEADER_DIRECTORIES return_list)
    FILE(GLOB_RECURSE new_list *.h)
    SET(dir_list "")
    FOREACH(file_path ${new_list})
        GET_FILENAME_COMPONENT(dir_path ${file_path} PATH)
        SET(dir_list ${dir_list} ${dir_path})
    ENDFOREACH()
    LIST(REMOVE_DUPLICATES dir_list)
    SET(${return_list} ${dir_list})
ENDMACRO()

HEADER_DIRECTORIES(header_dir_list)

#add all .h files so we can write "SomeClass.h" instead of "../../directrory/otherDirectory/SomeClass.h"
target_include_directories(${PROJECT_NAME} PUBLIC
  ${header_dir_list})

#add all .cpp and .h files to main target
target_sources(${PROJECT_NAME} PUBLIC
  ${AUTOMATIC_SCANNED_H_FILES}
  ${AUTOMATIC_SCANNED_CXX_FILES})
