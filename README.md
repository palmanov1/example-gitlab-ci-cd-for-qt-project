![Example GitLab CI/CD for Qt project](./pictures/banner.png)
# Example GitLab CI/CD for Qt project

This project is an example of how to do setup CI/CD for cross-platform Qt project.
The application itself doesn't do anything. `.gitlab-ci.yml` and Ruby scripts placed in `targets` sub directories are what you should be concerned about.


Watch our presentation about CI/CD setup for Qt projects: PLACE_LINK_HERE

See the blog post with the explanation: PLACE_LINK_HERE

---

[![Scythe Studio](./pictures/scythestudio-logo.png)](https://scythe-studio.com)

---

# About Scythe Studio
Zoho Books Forecasting application was developed and is maintained by Scythe Studio company.
We are an official Qt Service Partner and a provider of Qt Software Development services including:
- Desktop applications development
- Mobile applications development
- Embedded systems development
- Qt and C++ consulting
- UI/UX designing

Do not hesitate visting https://scythe-studio.com to discover our capabilities and learn more about Qt Software Development from [Scythe Studio Blog](https://scythe-studio.com/en/blog).