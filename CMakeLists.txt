cmake_minimum_required(VERSION 3.16)

project(CICDSetup VERSION 0.1 LANGUAGES CXX)
set(PRODUCT_BUNDLE_IDENTIFIER "com.scythestudio.test")


set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 11)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

#utility functions replacing "include_directory"
include("cmake/CMakeFunctions.cmake")
#Check this file for any *_DIR variable definitions and other
include("cmake/Locations.cmake")

list(APPEND REQUIRED_PACKAGES Core Quick Test)

find_package(QT NAMES Qt6 Qt5 COMPONENTS ${REQUIRED_PACKAGES} REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS ${REQUIRED_PACKAGES} REQUIRED)

if (WIN32 OR UNIX AND NOT IOS AND NOT ANDROID)
    enable_testing()
    add_subdirectory(tests)
endif()

list(APPEND REQUIRED_LINK_LIBS Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Quick Qt${QT_VERSION_MAJOR}::Test)

# add rcc instead of this
qt_add_resources(RSCS ${CMAKE_SOURCE_DIR}/src/resources.qrc)

if(MSVC OR MINGW)

    qt_add_executable(${PROJECT_NAME} WIN32 MANUAL_FINALIZATION ${RSCS})

elseif(UNIX AND NOT APPLE)

    qt_add_executable(${PROJECT_NAME} MANUAL_FINALIZATION ${RSCS})

    if(ANDROID)

        set_target_properties(${PROJECT_NAME} PROPERTIES
            QT_ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/targets/android"
        )

    endif()

elseif(IOS)
    # Kudos to https://github.com/OlivierLDff/QtIosCMake
    include(${PROJECT_SOURCE_DIR}/cmake/QtIosCMake/AddQt6IosApp.cmake)
    qt_add_executable(${PROJECT_NAME} MANUAL_FINALIZATION)

    if("$ENV{SHOULD_PACK}" STREQUAL "true")
        message("SHOULD_PACK ENABLED")
        add_qt_ios_app(${PROJECT_NAME}
            NAME ${PROJECT_NAME}
            BUNDLE_IDENTIFIER "com.scythestudio.cicdsample"
            LONG_VERSION 1.0
            ORIENTATION_PORTRAIT
            IPA
            UPLOAD_SYMBOL
            VERBOSE
            TEAM_ID $ENV{TEAM_ID}
        )
    else()
        message("SHOULD_PACK DISABLED")
        add_qt_ios_app(${PROJECT_NAME}
            NAME ${PROJECT_NAME}
            BUNDLE_IDENTIFIER "com.scythestudio.cicdsample"
            LONG_VERSION 1.0
            ORIENTATION_PORTRAIT
            TEAM_ID $ENV{TEAM_ID}
        )
        set_target_properties(${PROJECT_NAME} PROPERTIES
            XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY ""
            XCODE_ATTRIBUTE_CODE_SIGNING_ALLOWED "NO"
        )
    endif()

elseif(APPLE AND NOT IOS)

    qt_add_executable(${PROJECT_NAME} MACOSX_BUNDLE MANUAL_FINALIZATION ${RSCS})

else()

    message(FATAL_ERROR "Unsupported platform!")

endif()

add_subdirectory(src)

target_compile_definitions(${PROJECT_NAME}
    PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${PROJECT_NAME}
    PRIVATE ${REQUIRED_LINK_LIBS})

if(QT_VERSION_MAJOR EQUAL 6)
  qt_import_qml_plugins(${PROJECT_NAME})
  qt_finalize_executable(${PROJECT_NAME})
  qt_finalize_project(${PROJECT_NAME})
endif()
