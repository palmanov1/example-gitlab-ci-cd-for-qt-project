
# This script is used to build and pack iOS application.
# ------------------------------------------------------------
# To configure script, set these variables in the following way:
# export QT_DIR=/Users/lukasz/Qt/6.3.0/ios
require 'colorize'
require 'fileutils'
require 'optparse'

def showMessage(message)
    puts "------------------------------------------------------------------------------------------------------"
    puts "                                             #{message}"
    puts "------------------------------------------------------------------------------------------------------"
end

def showErrorMessage(message)
    puts "------------------------------------------------------------------------------------------------------".red
    puts "                                             #{message}".red
    puts "------------------------------------------------------------------------------------------------------".red
end

def executeCommand(command)
    result = %x[ #{command} ]
    showMessage(result)

    exitStatus = $?.exitstatus
    if exitStatus != 0
        showErrorMessage("Command \'#{command}\' failed!")
        exit 1
    end
    return result
end

#--------------------------------------------------------------------------------------------------
# SETUP
#--------------------------------------------------------------------------------------------------

showMessage(ENV.has_value?("QT_DIR"))

if "#{ENV["QT_DIR"]}".empty?
    showMessage("Set \"QT_DIR\" environment variable!")
    exit 1
end

project_dir = File.expand_path("#{__dir__}/../..")
build_dir = "#{project_dir}/build/iOS"
qt_dir = "#{ENV['QT_DIR']}/bin"
app_name = "CICDSetup"

should_build = ENV["SHOULD_BUILD"] == "true" || false
should_pack = ENV["SHOULD_PACK"] == "true" || false

ENV['CMAKE_PREFIX_PATH'] = ENV['QT_DIR']

showMessage("\nmiOS pipeline will run with the following setup\n
Project directory #{project_dir}\n
Build directory #{build_dir}\n
Qt directory #{qt_dir}\n
Should build? #{should_build}\n
Should pack? #{should_pack}\n
")

if should_build
    if Dir.exists?(build_dir)
        FileUtils.rm_rf(build_dir)
    end

    FileUtils.mkdir_p(build_dir)
    if not Dir.exists?(build_dir)
        showErrorMessage("Couldn't create build folder!")
        exit 1
    end

    showMessage("Compiling")
    FileUtils.cd(build_dir)

    project_dir = File.expand_path("#{__dir__}/../../")
    executeCommand("cmake -DCMAKE_PREFIX_PATH=#{qt_dir} \
    -DDEPLOYMENT_TARGET=14.0 \
    -DCMAKE_TOOLCHAIN_FILE:PATH=$QT_DIR/lib/cmake/Qt6/qt.toolchain.cmake \
    -DPLATFORM=OS64COMBINED \
    -DENABLE_BITCODE=FALSE \
    -GXcode \
    #{project_dir}")

    executeCommand("cmake --build . --config Release")
end

if should_pack
    if not Dir.exists?(build_dir)
        showErrorMessage("Can't find build folder!")
        exit 1
    end

    showMessage("Building for IPA #{ENV['TEAM_ID']}")
    FileUtils.cd(build_dir)

    project_dir = File.expand_path("#{__dir__}/../../")
    executeCommand("cmake -DCMAKE_PREFIX_PATH=#{qt_dir} \
    -DDEPLOYMENT_TARGET=14.0 \
    -DCMAKE_TOOLCHAIN_FILE:PATH=$QT_DIR/lib/cmake/Qt6/qt.toolchain.cmake \
    -DPLATFORM=OS64COMBINED \
    -DENABLE_BITCODE=FALSE \
    -GXcode \
    #{project_dir}")

    executeCommand("cmake --build . --config Release")

    if not File.exists?("#{build_dir}/#{app_name}Ipa/#{app_name}.ipa")
        showErrorMessage("Couldn't generate .ipa!")
        exit 1
    end
end